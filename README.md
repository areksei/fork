# Fork

_Fork_ - это model checker, который для многопоточного теста на C++ проверяет заданный инвариант во всех возможных состояниях, достижимых при исполнении этого теста. При нарушении инварианта model checker печатает кратчайшую детализированную траекторию, которая приводит к этому.

## Restrictions

- Подходит для проверки изолированных примитивов синхронизации или базовых инфраструктурных компонент, но не целых программ.
- Верифицирует только выполнение инвариантов (можно выразить `assert`-ом). _Liveness_ и более сложные _safety_ свойства не поддерживаются.
- “Разветвляет” исполнение только в точках обращения к примитивам синхронизации / атомикам.
- Поддерживает только последовательно согласованные программы (`memory_order == sec_cst`).


## Links

Постановка задачи, механизм работы, демонстрация работоспособности.

[Презентация](https://docs.google.com/presentation/d/1xjwP8-xH3mFM7QICKidUJEl4CEiJL297MwriOdZkOKk/edit?usp=sharing) (коротко)

[Диплом](https://drive.google.com/file/d/1Y3H8Bp4naqeVxUoX_rFOSiXmlNHmC7t2/view?usp=sharing) (подробно)

## Prerequisites 

OS: Linux

Libraries: binutils-dev

## Как запускать тесты

Если нет binutils-dev:  
`sudo apt-get install binutils-dev`

Чекер можно запустить в одном из двух режимов - _Explore_ и _Trace_.

### Режим _Explore_

В режиме _Explore_ чекер исследует все возможные траектории исполнения теста.

Запустите цель теста в _Debug_ /  _Release_ сборке.

В конце чекер напишет результат проверки: 
_All states discovered_ (чекер посетил все возможные траектории и не нашел нарушений инвариантов) 
или _Found expected/unexpected violation_ (чекер нашел нарушение инварианта).

### Режим _Trace_

В режиме _Trace_ чекер воспроизводит приводящую к нарушению инварианта траекторию, 
полученную в режиме _Explore_. 
В результате будет напечатан подробный трейс (разделяемое состояние, стеки потоков, локальные переменные).

Запустите ту же цель в _Debug_ сборке с флагом `-DFORK_TRACE=1`.

## Как писать тесты

- Тело теста писать внутри макроса `FORK_TEST`
- Для `thread`, `mutex`, `condvar`, `atomic`  заменить namespace `std` на `fork::stdlike`

### Настройка чекера

В начале теста чекеру можно передать указания:

```c++
fork::GetChecker()
    .SetUIntAtomicBound(3)
    .FailsExpected()
    .PrintState(
    [&lock, &in_critical_section]() {
      trace::StateDescription d{"LockState"};
      d.Add("TicketLock", lock);
      d.Add("in_critical_section_", in_critical_section);
      return d;
    });
```

Все указания - это методы синглтона `CheckerView` (с геттером `GetChecker()`):

#### Настройка _Explore_ 

`AddInvariant(MessagePredicate predicate)` - добавить инвариант для проверки чекером. 

Пример: 
```c++
fork::GetChecker()
      .AddInvariant([&]() -> std::pair<bool, std::string> {
        return {big != kWant, "Solved"};
      });
```

`Prune(Predicate predicate)` - при выполнении предиката убирать состояние из дальнейшего рассмотрения.

Пример:
```c++
fork::GetChecker()
    .Prune([&](){
      return next_free_ticket_.load() > 5;
    });
```

<!---
`ExpectDeadlock()` - при нахождении дедлока чекер напечатает _Found expected violation_ вместо _Found unexpected violation_.

`FailsExpected()` - при нахождении нарушения любого инварианта чекер напечатает _Found expected violation_ вместо _Found unexpected violation_.
-->

`SetUIntAtomicBound(UInt value)` - `atomic<UInt>` будет переполняться на `value`.

#### Настройка _Trace_

`PrintState(trace::Describer describer)` - указание, как печатать разделяемое состояние. 
Задается структурой `trace::StateDescription`.

Пример:
```c++
fork::GetChecker()
  .PrintState(
  [&lock, &in_critical_section]() {
    trace::StateDescription d{"LockState"};
    d.Add("TicketLock", lock);
    d.Add("in_critical_section", in_critical_section);
    return d;
  });
```

У `trace::StateDescription` есть метод `Add`, в который можно передать:
- интегральный тип
- строку
- атомик 
- объект с пользовательским 
  методом `trace::StateDescription DescribeState() const` - который по тому же принципу должен описать объект.
  
Чтобы не заниматься написанием `trace::StateDescription DescribeState() const` вручную,
к классу можно добавить макрос `FORK_DESCRIBE(field1, field2, field3, ...)` - 
он автоматически добавит метод с описанием переданных ему полей ([пример](https://gitlab.com/mary3000/fork/-/blob/d6acd315575c1c0de624a7fae171a3fac955b620/tests/lfstack_random/alloc_lfstack.hpp#L133)).

`ShowLocals(bool flag = true)` - включить/выключить отображение локальных переменных.

### Инструменты чекера

#### Инструменты _Explore_

`Fork()` - создает новое состояние (_форкается_) и кладет в очередь чекера.

`ForkGuarded<T>` - декоратор, отключающий форки на время вызова методов объекта `T`.

`FORK_ASSERT(condition, message)` - если `condition == false`, останавливает исполнение и печатает `message`.

`ATOMICALLY` - макрос для отключения форков в заданном блоке кода.

`Either()` - способ сделать развилку в тесте. Чекер проверит обе ветви исполнения.

Пример:
```c++
if (fork::Either()) {
  lock.Lock();
} else {
  lock.TryLock();
}
```

`Random(n)` - обобщение `Either`. Генерирует "случайное число" от 0 до n включительно и создает n+1 развилку - 
по одной на каждый выход `Random`.

`Prune()` - убирает состояние из дальнейшего рассмотрения.

#### Инструменты _Trace_

`FORK_NOTE(message)` - заметка о событии для детализации трейса.

`ShowLocalState() / ShowState()` - показать в трейсе локальное / разделямое состояние в текущий момент времени.
