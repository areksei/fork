#include <fork/core/allocation/allocator.hpp>
#include <fork/core/storage/disk_based.hpp>
#include <fork/core/checker.hpp>

#include <cstring>
#include <cassert>

namespace fork {

//////////////////////////////////////////////////////////////////////

Allocator::Snapshot Allocator::CreateSnapshot() const {
  Snapshot snapshot;

  // Save arena content
  size_t arena_size = break_ - AsMemSpan().Begin();
  wheels::MutableMemView arena{Begin(), arena_size};
  GetCurrentChecker()->AllocatorStorage().Assign(snapshot.arena_, arena);

  // Save other fields
  memcpy(&snapshot.this_, &this_, sizeof(AllocatorFields));

  return snapshot;
}

void Allocator::Restore(const Snapshot& snapshot) {
  // Restore arena content
  memcpy(Begin(), snapshot.arena_.Begin(), snapshot.arena_.Size());

  // Restore other fields
  memcpy(&this_, &snapshot.this_, sizeof(AllocatorFields));

  break_ = AsMemSpan().Begin() + snapshot.arena_.Size();
}

//////////////////////////////////////////////////////////////////////

static const int kAlignment = __STDCPP_DEFAULT_NEW_ALIGNMENT__;

void Allocator::Init(wheels::MmapAllocation arena) {
  arena_ = std::move(arena);
  memset(arena_.Start(), 0, arena_.Size());
  break_ = arena_.Start();
  AlignFirstAllocation();
}

void* Allocator::Allocate(size_t size) {
  size = AlignSize(size);

  BlockNode* next;
  BlockNode* prev;
  GetBlock(size, prev, next);
  Remove(prev, next);

  return reinterpret_cast<char*>(next) + kHeaderSize;
}

void Allocator::Remove(BlockNode* prev, const BlockNode* next) {
  if (next == this_.head_) {
    this_.head_ = this_.head_->next_;
  } else {
    prev->next_ = next->next_;
  }
}

void Allocator::GetBlock(size_t size, BlockNode*& prev, BlockNode*& next) {
  next = this_.head_;
  prev = next;
  while (next != nullptr && next->size_ < size) {
    prev = next;
    next = next->next_;
  }

  if (next == nullptr) {
    size_t total_size = size + kHeaderSize;
    WHEELS_VERIFY(break_ + total_size < arena_.End(), "Exceeded arena size");

    // Get rid of dirty memory from previous allocator state
    memset(break_, 0, total_size);

    BlockNode* head = this_.head_;
    this_.head_ = reinterpret_cast<BlockNode*>(break_);
    this_.head_->next_ = head;
    this_.head_->size_ = size;

    next = prev = this_.head_;

    assert((size_t)(break_ + kHeaderSize) % kAlignment == 0);
    break_ += total_size;
    assert((size_t)(break_ + kHeaderSize) % kAlignment == 0);
  }
}

void Allocator::Free(void* pointer) {
  WHEELS_VERIFY(
      pointer >= arena_.Start() + kHeaderSize && pointer < arena_.End(),
      "Pointer is not from arena");

  auto node =
      reinterpret_cast<BlockNode*>((static_cast<char*>(pointer) - kHeaderSize));
  size_t size = node->size_;
  memset(pointer, 0, size);

  node->next_ = this_.head_;
  this_.head_ = node;
}

void Allocator::AlignFirstAllocation() {
  size_t to_align = (size_t)break_ + kHeaderSize;
  size_t r = to_align % kAlignment;
  break_ += (kAlignment - r);
  assert(((size_t)break_ + kHeaderSize) % kAlignment == 0);
}

size_t Allocator::AlignSize(size_t size) {
  size_t to_align = size + kHeaderSize;
  size_t r = to_align % kAlignment;
  size_t addition = (kAlignment - r);
  assert((size + addition + kHeaderSize) % kAlignment == 0);
  return size + addition;
}

//////////////////////////////////////////////////////////////////////

}  // namespace fork
