#pragma once

#include <wheels/memory/mmap_allocation.hpp>
#include <wheels/memory/view.hpp>

#include <fork/core/hash.hpp>
#include <fork/core/storage/disk_based.hpp>

#include <vector>
#include <iostream>

namespace fork {

//////////////////////////////////////////////////////////////////////

struct BlockNode {
  size_t size_;
  BlockNode* next_;
};

struct AllocatorFields {
  BlockNode* head_{nullptr};
};

//////////////////////////////////////////////////////////////////////

static const size_t kPageCount = 8;

class Allocator {
  static const size_t kHeaderSize = sizeof(BlockNode::size_);

 public:
  class Snapshot {
   public:
    Snapshot() = default;

    Snapshot(Snapshot&& other) {
      *this = std::move(other);
    }

    Snapshot& operator=(Snapshot&& other) {
      arena_ = other.arena_;
      this_ = other.this_;
      other.arena_ = {};

      return *this;
    }

    Snapshot(const Snapshot& other) = default;
    Snapshot& operator=(const Snapshot& other) = default;

    ~Snapshot() {
      storage::DiskFree(arena_.Data());
    }

    size_t Size() const {
      return arena_.Size();
    }

   private:
    wheels::MutableMemView arena_;
    AllocatorFields this_;

    friend Allocator;
  };

 public:
  // For model checker
  Snapshot CreateSnapshot() const;
  void Restore(const Snapshot& snapshot);

 public:
  void Init(wheels::MmapAllocation arena =
                wheels::MmapAllocation::AllocatePages(kPageCount));

  void* Allocate(size_t size);

  void Free(void* pointer);

  void Reset() {
    this_ = {};
    break_ = arena_.Start();
  }

  char* Begin() const {
    return arena_.Start();
  }

  void FeedHasher(hash::Hasher& hasher) const {
    hasher.Absorb(Begin(), break_ - Begin());
  }

 private:
  wheels::ConstMemView AsMemSpan() const {
    return arena_.View();
  }

  wheels::MutableMemView AsMemSpan() {
    return arena_.View();
  }

  void GetBlock(size_t size, BlockNode*& prev, BlockNode*& next);
  void Remove(BlockNode* prev, const BlockNode* next);

  void AlignFirstAllocation();
  size_t AlignSize(size_t size);

 private:
  wheels::MmapAllocation arena_;
  AllocatorFields this_;
  char* break_;
};

//////////////////////////////////////////////////////////////////////

}  // namespace fork
