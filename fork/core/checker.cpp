#include <fork/core/checker.hpp>
#include <fork/core/storage/disk_based.hpp>
#include <fork/core/exception.hpp>
#include <fork/test/fork.hpp>

#include <wheels/support/assert.hpp>
#include <wheels/support/env.hpp>
#include <wheels/logging/logging.hpp>

#include <cstring>
#include <fstream>

#include <sys/stat.h>
#include <sys/types.h>

namespace fork {

//////////////////////////////////////////////////////////////////////

static thread_local Fiber* current_fiber;

Fiber* GetCurrentFiber() {
  return current_fiber;
}

static inline Fiber* GetAndResetCurrentFiber() {
  auto* f = current_fiber;
  current_fiber = nullptr;
  return f;
}

static inline void SetCurrentFiber(Fiber* f) {
  current_fiber = f;
}

//////////////////////////////////////////////////////////////////////

static thread_local Checker* current_checker;

Checker* GetCurrentChecker() {
  WHEELS_VERIFY(current_checker, "not in model checker");
  return current_checker;
}

bool IsRunningInChecker() {
  return current_fiber != nullptr;
}

struct CheckerScope {
  explicit CheckerScope(Checker* checker) {
    WHEELS_VERIFY(!current_checker,
                  "cannot run model checker from another model checker");
    current_checker = checker;
  }

  ~CheckerScope() {
    current_checker = nullptr;
  }
};

void Checker::Run(FiberRoutinePtr main) {
  CheckerScope scope(this);
  Setup();
  try {
#if defined(FORK_TRACE)
    Trace(main);
#else
    Init(main);
    RunLoop();
#endif
  } catch (Expected e) {
    // std::cout << e.what() << std::endl;
  }
}

void Checker::RunLoop() {
  timer_.Restart();
  heartbeat_.Restart();

  while (states_.HasMore() && depth_ < max_depth_) {
    State state = states_.Pop();

    size_t depth = state.Path().Size();
    if (depth > depth_) {
      depth_ = depth;
      storage::DiskReleaseOld();

      if (heartbeat_.Elapsed() >= std::chrono::seconds(1)) {
        heartbeat_.Restart();
        ReportStats(false, state.PathString());
      }

      states_.ForgetFront();
    }

    for (size_t i = 0; i < fibers_.size(); ++i) {
      if (!state.Enabled(i)) {
        continue;
      }
      Step(state, i);
    }
  }

  ReportStats(true);
  if (config_.FailsExpected()) {
    throw Unexpected("Violations not found");
  }
}

void Checker::CheckException(const State& state, size_t i) {
  if (config_.State() == Failed) {
    auto path = state.PathString() + std::to_string(i);
    ReportStats(true, path);
    WriteViolationPath(path);
    if (config_.Exception()) {
      std::rethrow_exception(config_.Exception());
    } else {
      throw Expected(config_.Message());
    }
  }
}

MessagePredicate Checker::CheckDeadlock() {
  return [this]() -> std::pair<bool, std::string> {
    bool finished = true;
    bool continuable = false;
    std::stringstream report;
    report << "Deadlock detected, suspended threads: ";

    for (auto fiber : fibers_) {
      auto state = fiber->State();
      if (state != FiberState::Terminated) {
        finished = false;
      }
      if (fiber->Enabled()) {
        continuable = true;
        break;
      }
      if (fiber->State() == FiberState::Suspended) {
        report << fiber->Id() << " ";
      }
    }

    return {finished || continuable, report.str()};
  };
}

void Checker::Init(FiberRoutinePtr main) {
  Spawn(main);
  states_.Push(Snapshot({}));
}

void Checker::Spawn(FiberRoutinePtr function) {
  if (existing_fiber_count_ < fibers_.size()) {
    fibers_[existing_fiber_count_]->Init(function);
  } else {
    Fiber* fiber = Fiber::Create(function);
    fibers_.push_back(fiber);
  }
  ++existing_fiber_count_;
}

State Checker::Snapshot(State::TPath path) const {
  return State::Create(fibers_, existing_fiber_count_, allocator_,
                       std::move(path));
}

void Checker::Step(State& prev, size_t index) {
  Restore(prev);
  SwitchTo(fibers_[index]);

  int random = fibers_[index]->GetRandom();
  if (random == -1) {
    UpdateStates(prev, index);
  } else {
    State fork = Snapshot(prev.Path());
    for (; random >= 0; --random) {
      fibers_[index]->SetRandom(random);
      SwitchTo(fibers_[index]);
      UpdateStates(fork, index * 10 + random);
      Restore(fork);
    }
  }
}

void Checker::Yield() {
  if (!IsRunningInChecker()) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToChecker();
}

void Checker::SleepFor(wheels::Duration duration) {
  WHEELS_UNUSED(duration);
  WHEELS_UNREACHABLE();
}

void Checker::Suspend() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Suspended);
  SwitchToChecker();
}

void Checker::Suspend(FiberDescriptor ptr) {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(ptr);

  // FORK_INTERNAL_NOTE("Park fiber " << fork::GetCurrentFiber()->Id() << " ("
  // << Info(ptr) << ")");

  SwitchToChecker();
}

void Checker::Resume(Fiber* that) {
  that->SetState(FiberState::Runnable);
}

void Checker::Terminate() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Terminated);
  SwitchToChecker();
}

void Checker::RestartFiber() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Restarted);
  SwitchToChecker();
}

void Checker::SwitchTo(Fiber* fiber) {
  config_.Reset();

  SetCurrentFiber(fiber);
  fiber->SetState(FiberState::Running);

  loop_context_.SwitchTo(fiber->Context());

  if (fiber->State() == FiberState::Restarted) {
    size_t bytes = 1024;
    memset(fiber->GetStack().View().Back() - bytes, 0, bytes);
    fiber->Init(fiber->UserRoutinePtr());
    SetCurrentFiber(fiber);
    fiber->SetState(FiberState::Running);
    loop_context_.SwitchTo(fiber->Context());
  }
}

void Checker::SwitchToChecker(bool final) {
#if defined(FORK_TRACE)
  tracer_.TraceAfter();
#endif

  // Need to force forks here: imagine ForkGuarded blocking queue:
  // if it tries to lock already taken mutex, it has to switch to checker
  FORCE_FORKS {
    Fiber* caller = GetAndResetCurrentFiber();
    if (final) {
      caller->Context().ExitTo(loop_context_);
    } else {
      caller->Context().SwitchTo(loop_context_);
    }
  }

#if defined(FORK_TRACE)
  tracer_.TraceBefore();
#endif
}

void Checker::UpdateStates(State& prev, size_t index) {
  config_.Run();
  CheckException(prev, index);

  if (config_.State() == Pruned) {
    ++pruned_count_;
    return;
  }

  auto state_hash = HashState(true);
  // std::cout << "hash: " << state_hash << std::endl;
  if (!states_.Contains(state_hash)/* || true*/) {
    // std::cout << "new" << std::endl;
    states_.Push(Snapshot(prev.NewPath(index)));
  } else {
    // std::cout << "dup" << std::endl;
  }
}

void* Checker::Allocate(size_t size, bool faulty) {
  if (faulty) {
    Fork();
  }
  return allocator_.Allocate(size);
}

void Checker::Free(void* pointer, bool faulty) {
  if (faulty) {
    Fork();
  }
  allocator_.Free(pointer);
}

void Checker::Setup() {
  allocator_.Init();
  storage::DiskInit();
  config_.AddInvariant(CheckDeadlock());
}

void Checker::OnFailure(std::exception_ptr exception) {
  config_.OnException(exception);
}

void Checker::OnFailure(const std::string& message) {
  config_.OnFailure(message);
  Terminate();
}

void Checker::Restore(const State& prev) {
  existing_fiber_count_ = prev.FiberCount();

  for (size_t i = 0; i < existing_fiber_count_; ++i) {
    fibers_[i]->Restore(prev.Fibers()[i]);
  }

  allocator_.Restore(prev.AllocatorSnapshot());
}

std::vector<int> ParsePath(std::stringstream& stream) {
  std::vector<int> path;
  std::string token;
  while (stream >> token) {
    path.push_back(std::atoi(token.c_str()));
  }
  return path;
}

std::vector<int> Checker::ReadViolationPath() const {
  std::ifstream file(path_addr_);
  std::stringstream sstr;
  sstr << file.rdbuf();

  return ParsePath(sstr);
}

void Checker::Trace(FiberRoutinePtr main) {
  Spawn(main);

  while (Fiber* fiber = tracer_.Next()) {
    SwitchTo(fiber);

    config_.Run();
    if (config_.State() == Failed) {
      tracer_.Finish(config_.Message());

      if (config_.Exception()) {
        std::rethrow_exception(config_.Exception());
      } else {
        throw Expected(config_.Message());
      }
    }
  }
  throw Unexpected("Violations not found");
}

void PushDown(std::vector<hash::THash>& hashes, size_t j) {
  while (j > 0 && hashes[j] < hashes[j - 1]) {
    std::swap(hashes[j], hashes[j - 1]);
    --j;
  }
}

hash::THash Checker::HashState(bool symmetric) const {
  hash::Hasher hasher;

  static std::vector<hash::THash> hashes(4);

  if (symmetric) {
    hashes.resize(existing_fiber_count_);

    for (size_t i = 0; i < existing_fiber_count_; ++i) {
      // std::cout << i << std::endl;
      hashes[i] = fibers_[i]->Hash(true);
      PushDown(hashes, i);
    }

    hasher.Absorb(hashes.data(), sizeof(hash::THash) * hashes.size());
  } else {
    for (size_t i = 0; i < existing_fiber_count_; ++i) {
      hasher.Absorb(*fibers_[i]);
    }
  }

  hasher.Absorb(allocator_);

  return hasher.Hash();
}

int Checker::Random(int max) {
  // TWIST_VERIFY(GetFiberId() != 0, "random for first fiber not supported");
  if (max == 0 || GetFiberId() == 0) {
    return 0;
  }
#if !defined(FORK_TRACE)
  GetCurrentFiber()->SetRandom(max);
  Yield();
#endif
  return GetCurrentFiber()->GetAndResetRandom();
}

void Checker::ReportStats(bool final, const std::string& path) const {
  if (final) {
    if (config_.State() == Failed) {
      std::string status = config_.Exception() ? "unexpected" : "expected";
      std::cout << "Found " + status + " violation: " << config_.Message()
                << std::endl;
    } else if (!states_.HasMore()) {
      std::cout << "All states discovered" << std::endl;
    } else {
      std::cout << "Out of fuel" << std::endl;
    }
  }

  std::cout << "Diameter " << depth_ << std::endl;
  states_.PrintStatistics();
  std::cout << "Pruned " << pruned_count_ << std::endl;

  if (config_.State() == Failed) {
    std::cout << "Violation path written to " << path_addr_ << ":" << std::endl;
  }
  if (!path.empty()) {
    std::cout << path << std::endl;
  }
  std::cout << "Elapsed " << timer_.Elapsed().count() / 1000000.0 << " ms"
            << std::endl;
  std::cout << std::endl;
}

Invariant& Checker::AddInvariant(MessagePredicate check) {
  AllocationGuard guard;
  return config_.AddInvariant(std::move(check));
}

void Checker::AddPrune(std::function<bool()> prune) {
  AllocationGuard guard;
  config_.AddPrune(std::move(prune));
}

// todo: to wheels?
bool CreateDir(const std::string& dir) {
  return mkdir(dir.data(), 0700) == 0;
}

// todo: to wheels?
void CreateDirectories(const std::string& path) {
  size_t pos = 0;
  pos = path.find('/', pos + 1);

  while (pos != std::string::npos) {
    CreateDir(path.substr(0, pos));
    pos = path.find('/', pos + 1);
  }
}

void Checker::WriteViolationPath(const std::string& path) {
  CreateDirectories(path_addr_);
  std::ofstream out(path_addr_);
  out << path;
}

void Trace(const std::string& path) {
  GetCurrentChecker()->Trace(path);
}

void Checker::Trace(const std::string& path) {
  AllocationGuard g;
  path_addr_ = path;

#if defined(FORK_TRACE)
  tracer_.StorePath(ReadViolationPath());
#endif
}

void Checker::Fork() {
  if (!IsRunningInChecker() || forks_disabled_) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToChecker();
}

void Checker::ExpectDeadlock() {
  config_.Expect(0);
}

void Checker::FailsExpected() {
  config_.ExpectFails();
}

void Checker::SetDepth(size_t depth) {
  max_depth_ = depth;
}

void Spawn(FiberRoutine* routine) {
  AllocationGuard guard;
  GetCurrentChecker()->Spawn(routine);
}

std::string WaitQueueString(FiberDescriptor ptr) {
  std::stringstream queue;
  queue << "wait queue: ";

  auto fibers = GetCurrentChecker()->GetSuspendedFibers(ptr);

  for (size_t i = 0; i < fibers.size(); ++i) {
    queue << fibers[i]->Id();
    if (i != fibers.size() - 1) {
      queue << " -> ";
    }
  }

  if (fibers.empty()) {
    queue << "<empty>";
  }

  return queue.str();
}

void Fork() {
  GetCurrentChecker()->Fork();
}

AllocationGuard::AllocationGuard() {
  fiber_ = GetAndResetCurrentFiber();
}

AllocationGuard::~AllocationGuard() {
  SetCurrentFiber(fiber_);
}

ForkGuard::ForkGuard(bool forks_disabled) {
  prev_preempt_ = GetCurrentChecker()->PreemptDisable(forks_disabled);
}

ForkGuard::~ForkGuard() {
  GetCurrentChecker()->PreemptDisable(prev_preempt_);
}

}  // namespace fork
