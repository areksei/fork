#pragma once

#include <fork/core/fiber.hpp>
#include <fork/core/state.hpp>
#include <fork/core/allocation/allocator.hpp>
#include <fork/core/state_queue.hpp>
#include <fork/core/config.hpp>
#include <fork/core/storage/snapshot_storage.hpp>
#include <fork/core/trace/trace.hpp>

#include <wheels/support/env.hpp>

#include <vector>
#include <unordered_set>

namespace fork {

class Checker {
 public:
  // Schedule api
  void Run(FiberRoutinePtr main);
  void Yield();
  void SleepFor(wheels::Duration duration);
  void Suspend();
  void Suspend(FiberDescriptor ptr);
  void Resume(Fiber* that);
  void Terminate();
  void RunLoop();
  void Spawn(FiberRoutinePtr function);

  void Fork();

  void RestartFiber();

  void* Allocate(size_t size, bool faulty = false);
  void Free(void* pointer, bool faulty = false);

  void Setup();

  void Trace(const std::string& path);

  void OnFailure(std::exception_ptr exception);
  void OnFailure(const std::string& message);

  Invariant& AddInvariant(MessagePredicate check);
  void AddPrune(std::function<bool()> prune);

  int Random(int max);

  void ExpectDeadlock();
  void FailsExpected();

  void SetDepth(size_t depth);

  bool PreemptDisable(bool disable) {
    return std::exchange(forks_disabled_, disable);
  }

  void Prune() {
    config_.OnPruned();
    SwitchToChecker();
  }

  storage::PointerStorage& StackStorage() {
    return states_.StackStorage();
  }

  storage::PointerStorage& AllocatorStorage() {
    return states_.AllocatorStorage();
  }

  FiberPtrs GetSuspendedFibers(FiberDescriptor ptr) const {
    FiberPtrs suspended;
    for (size_t i = 0; i < existing_fiber_count_; ++i) {
      if (fibers_[i]->Descriptor() == ptr) {
        suspended.push_back(fibers_[i]);
      }
    }
    return suspended;
  }

  void PrintState(trace::Describer printer) {
    tracer_.AddPrinter(std::move(printer));
  }

  trace::Tracer& GetTracer() {
    return tracer_;
  }

  bool ForksDisabled() const {
    return forks_disabled_;
  }

  size_t NextId() {
    return sequencer_.Next();
  }

 private:
  void Init(FiberRoutinePtr main);

  void Step(State& prev, size_t index);
  void Restore(const State& prev);
  void UpdateStates(State& prev, size_t index);

  State Snapshot(State::TPath path) const;

  void CheckException(const State& state, size_t i);
  MessagePredicate CheckDeadlock();

  void SwitchTo(Fiber* fiber);
  void SwitchToChecker(bool final = false);

  void ReportStats(bool final, const std::string& path = "") const;

  hash::THash HashState(bool symmetric) const;

  void Trace(FiberRoutinePtr main);
  std::vector<int> ReadViolationPath() const;
  void WriteViolationPath(const std::string& path);

 private:
  // todo: extract to UnpackedState
  context::ExecutionContext loop_context_;
  FiberPtrs fibers_;
  size_t existing_fiber_count_{0};
  Allocator allocator_;

  storage::SnapshotStorage states_;

  size_t pruned_count_{0};
  size_t depth_{0};

  wheels::StopWatch<> timer_;
  // todo: add exponential stats printer?
  wheels::StopWatch<> heartbeat_;

  size_t max_depth_{static_cast<size_t>(-1)};

  bool forks_disabled_{false};

  Config config_;

  // todo: path and Read/Write path to Tracer
  std::string path_addr_{wheels::GetTempPath().value_or("/tmp") +
                         "/fork/tests/unknown.log"};

  trace::Tracer tracer_{fibers_};

  struct Sequencer {
    size_t Next() {
      return counter_++;
    }

    size_t counter_{0};
  };
  Sequencer sequencer_;
};

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();
Checker* GetCurrentChecker();

void Spawn(FiberRoutinePtr routine_ptr);

bool IsRunningInChecker();

void Fork();

/////////////////////////////////////////////////////////////////////

#if defined(FORK_TRACE)
#define SHOW_QUEUE(ptr) WaitQueueString(ptr)
#else
#define SHOW_QUEUE(ptr)
#endif

std::string WaitQueueString(FiberDescriptor ptr);

class AllocationGuard {
 public:
  AllocationGuard();
  ~AllocationGuard();

 private:
  Fiber* fiber_;
};

}  // namespace fork
