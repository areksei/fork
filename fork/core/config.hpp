#pragma once

#include <fork/test/detail.hpp>

#include <functional>
#include <vector>

namespace fork {

enum ConfigState { None, Failed, Pruned };

using Predicate = std::function<bool()>;
using MessagePredicate = std::function<std::pair<bool, std::string>()>;

class Config {
 public:
  ConfigState State() const {
    return state_;
  }

  std::exception_ptr Exception() const {
    return exception_;
  }

  const std::string& Message() const {
    return message_;
  }

  Invariant& AddInvariant(MessagePredicate predicate) {
    invariants_.push_back(Invariant{std::move(predicate)});
    return invariants_.back();
  }

  void AddPrune(Predicate predicate) {
    prunes_.push_back(std::move(predicate));
  }

  void Reset() {
    state_ = None;
    exception_ = nullptr;
  }

  void OnException(std::exception_ptr exception) {
    state_ = Failed;
    exception_ = exception;
  }

  void OnFailure(const std::string& message);

  void OnPruned() {
    state_ = Pruned;
  }

  void Expect(size_t index) {
    fails_expected_ = true;
    invariants_[index].fail_expected_ = true;
  }

  void ExpectFails() {
    fails_expected_ = true;
  }

  bool FailsExpected() {
    return fails_expected_;
  }

  void Run();

 private:
  bool CheckInvariants();
  bool RunPrunes();

 private:
  ConfigState state_{None};

  // todo: separate exception and invariant violation
  std::exception_ptr exception_{nullptr};
  std::string message_{};

  std::vector<Invariant> invariants_;
  std::vector<Predicate> prunes_;

  bool fails_expected_{false};
};

}  // namespace fork
