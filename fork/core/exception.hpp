#pragma once

#include <stdexcept>

namespace fork {

class Unexpected : public std::runtime_error {
 public:
  Unexpected(const std::string& message) : std::runtime_error(message) {
  }
};

class Expected : public std::runtime_error {
 public:
  Expected(const std::string& message) : std::runtime_error(message) {
  }
};

}  // namespace fork
