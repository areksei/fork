#pragma once

#include <context/context.hpp>
#include <context/stack.hpp>

#include <fork/test/detail.hpp>
#include <fork/core/hash.hpp>
#include <fork/core/storage/disk_based.hpp>

#include <wheels/intrusive/list.hpp>
#include <wheels/support/time.hpp>

#include <utility>
#include <cassert>
#include <cstring>

namespace fork {

class Fiber;

using FiberPtrs = std::vector<Fiber*>;
using FiberRoutinePtr = FiberRoutine*;

//////////////////////////////////////////////////////////////////////

using FiberDescriptor = uintptr_t;
enum class FiberState {
  Starting,
  Runnable,
  Running,
  Terminated,
  Restarted,
  Suspended
};

inline std::string StateString(FiberState state) {
  switch (state) {
    case FiberState::Starting:
      return "STARTING";
    case FiberState::Runnable:
      return "RUNNABLE";
    case FiberState::Running:
      return "RUNNING";
    case FiberState::Terminated:
      return "TERMINATED";
    case FiberState::Restarted:
      return "RESTARTED";
    case FiberState::Suspended:
      return "SUSPENDED";
    default:
      WHEELS_UNREACHABLE();
  }
}

inline bool EnabledState(FiberState state) {
  return !(state == FiberState::Terminated || state == FiberState::Suspended);
}

static const auto kMaxState =
    static_cast<FiberDescriptor>(FiberState::Suspended);

inline FiberState ToState(FiberDescriptor d) {
  if (d <= kMaxState) {
    return static_cast<FiberState>(d);
  }
  return FiberState::Suspended;
}

//////////////////////////////////////////////////////////////////////

struct FiberFields {
  FiberDescriptor state_;
  int random_{-1};
};

//////////////////////////////////////////////////////////////////////

class Fiber {
 public:
  size_t Id() const {
    return id_;
  }

  context::Stack& GetStack() {
    return stack_;
  }

  context::ExecutionContext& Context() {
    return context_;
  }

  FiberState State() const {
    return ToState(this_.state_);
  }

  FiberDescriptor Descriptor() const {
    return this_.state_;
  }

  void SetState(FiberState target) {
    SetState(static_cast<FiberDescriptor>(target));
  }

  void SetState(FiberDescriptor target) {
    this_.state_ = target;
  }

  FiberRoutinePtr UserRoutinePtr() {
    return routine_ptr_;
  }

  FiberRoutine UserRoutine() __attribute__((noinline)) {
    return *routine_ptr_;
  }

  void InvokeRoutine() {
    (*routine_ptr_)();
  }

  void SetUserRoutinePtr(FiberRoutinePtr routine_ptr) {
    routine_ptr_ = routine_ptr;
  }

  size_t StackSize() const {
    return stack_end_ - (char*)context_.machine_ctx_.rsp_;
  }

  bool Enabled() const {
    return EnabledState(ToState(this_.state_));
  }

  void Init(FiberRoutinePtr routine_ptr);

  static Fiber* Create(FiberRoutinePtr routine_ptr);
  static void SetupTrampoline(Fiber* fiber);

  hash::THash Hash(bool symmetric);
  void FeedHasher(hash::Hasher& hasher, bool symmetric = false) const;
  char* CanonizeStack() const;

  void PrintStack() const {
    Print(context_.machine_ctx_.rsp_, StackSize());
  }

  void Print(void* stack, size_t size) const {
    std::cout << this << std::endl;
    uint64_t* data = (uint64_t*)stack;
    for (size_t i = 0; i < size / 8; ++i) {
      std::cout << std::hex << data[i] << " ";
    }
    std::cout << std::dec;
    std::cout << std::endl;
  }

  int GetRandom() const {
    return this_.random_;
  }

  int GetAndResetRandom() {
    int random = this_.random_;
    this_.random_ = -1;
    return random;
  }

  void SetRandom(int random) {
    this_.random_ = random;
  }

  bool NextFork() {
    if (this_.random_ != -1) {
      --this_.random_;
    }
    return this_.random_ != -1;
  }

  void CanonizeValues(wheels::MutableMemView values) {
    to_canonize_ = values;
  }

  std::string& Where() {
    return where_;
  }

  FiberId& Initiator() {
    return initiator_;
  }

  class Snapshot;
  Snapshot CreateSnapshot();
  void Restore(const Snapshot& snapshot);

 public:
  class Snapshot {
   public:
    Snapshot(Snapshot&& other) {
      this_ = other.this_;
      stack_ = other.stack_;
      other.stack_ = {};
    }

    Snapshot& operator=(Snapshot&& other) = default;
    Snapshot& operator=(const Snapshot& other) = default;
    Snapshot(const Snapshot& other) = default;

    ~Snapshot() {
      storage::DiskFree(stack_.Data());
    }

    size_t Size() const {
      return stack_.Size();
    }

    bool Enabled() const {
      return EnabledState(ToState(this_.state_));
    }

    Snapshot() = default;

   private:
    wheels::MutableMemView stack_;
    FiberFields this_;

    FiberRoutinePtr routine_ptr_;

    friend Fiber;
  };

 private:
  context::Stack stack_;
  FiberFields this_;

  FiberRoutinePtr routine_ptr_;

  FiberId id_;
  context::ExecutionContext context_;
  char* stack_end_;

  wheels::MutableMemView to_canonize_{};

  std::string where_;
  FiberId initiator_{0};
};

//////////////////////////////////////////////////////////////////////

void Spawn(FiberRoutine routine);

void Yield();

void SleepFor(wheels::Duration duration);

FiberId GetFiberId();

//////////////////////////////////////////////////////////////////////

}  // namespace fork
