#include <fork/core/state.hpp>

#include <cstring>

namespace fork {

State State::Create(const FiberPtrs& fibers, size_t existing_fiber_count,
                    const Allocator& allocator, TPath path) {
  State state;

  state.fiber_snapshots_ = reinterpret_cast<FiberSnapshots>(
      storage::DiskAllocate(sizeof(Fiber::Snapshot) * existing_fiber_count));
  for (size_t i = 0; i < existing_fiber_count; ++i) {
    state.fiber_snapshots_[i] = fibers[i]->CreateSnapshot();
  }

  state.fiber_count_ = existing_fiber_count;
  state.allocator_snapshot_ = allocator.CreateSnapshot();
  state.path_ = path;

  return state;
}

}  // namespace fork
