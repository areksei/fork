#pragma once

#include <fork/core/fiber.hpp>
#include <functional>
#include <cassert>
#include <wheels/logging/logging.hpp>
#include <fork/core/allocation/allocator.hpp>

namespace fork {

// Doesn't hold ownership. rename to StateView?
class State : public wheels::IntrusiveListNode<State> {
 public:
  using TPath = wheels::MutableMemView;
  using FiberSnapshots = Fiber::Snapshot*;

  static State Create(const FiberPtrs& fibers, size_t existing_fiber_count,
                      const Allocator& allocator, TPath path);

  bool Enabled(size_t index) const {
    return index < fiber_count_ && fiber_snapshots_[index].Enabled();
  }

  size_t FiberCount() const {
    return fiber_count_;
  }

  const FiberSnapshots& Fibers() const {
    return fiber_snapshots_;
  }

  const Allocator::Snapshot& AllocatorSnapshot() const {
    return allocator_snapshot_;
  }

  const TPath& Path() const {
    return path_;
  }

  TPath NewPath(size_t index) const {
    auto data = storage::DiskAllocate(path_.Size() + 1);
    memcpy(data, path_.Data(), path_.Size());
    data[path_.Size()] = index;
    return {data, path_.Size() + 1};
  }

  std::string PathString() const {
    std::stringstream path_string;
    for (size_t i = 0; i < path_.Size(); ++i) {
      path_string << std::to_string(path_.Data()[i]) + " ";
    }
    return path_string.str();
  }

 private:
  FiberSnapshots fiber_snapshots_;
  size_t fiber_count_{0};
  Allocator::Snapshot allocator_snapshot_;
  TPath path_;
};

}  // namespace fork
