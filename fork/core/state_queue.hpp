#pragma once

#include <fork/core/state.hpp>

#include <queue>

namespace fork {

/////////////////////////////////////////////////////////////////////

using StatePtr = State*;

class StateQueue {
 public:
  virtual ~StateQueue() = default;

  virtual State Pop() = 0;
  virtual void Push(State state) = 0;
  virtual void Clear() = 0;

  virtual bool IsEmpty() const = 0;
  virtual size_t Size() const = 0;
};

/////////////////////////////////////////////////////////////////////

class PtrStateQueue : public StateQueue {
 public:
  State Pop() override {
    StatePtr ptr = states_.front();
    states_.pop_front();
    return *ptr;
  }

  void Push(State state) override {
    char* place = storage::DiskAllocate(sizeof(state));
    memcpy(place, static_cast<void*>(&state), sizeof(state));
    states_.push_back(reinterpret_cast<StatePtr>(place));
  }

  void Clear() override {
    states_.clear();
  }

  bool IsEmpty() const override {
    return states_.empty();
  }

  size_t Size() const override {
    return states_.size();
  }

 private:
  std::deque<StatePtr> states_;
};

/////////////////////////////////////////////////////////////////////

class IntrusiveStateQueue : public StateQueue {
 public:
  State Pop() override {
    StatePtr ptr = states_.PopFront();
    --size_;
    return *ptr;
  }

  void Push(State state) override {
    char* place = storage::DiskAllocate(sizeof(state));
    memcpy(place, static_cast<void*>(&state), sizeof(state));
    auto state_ptr = reinterpret_cast<StatePtr>(place);
    state_ptr->next_ = state_ptr->prev_ = nullptr;
    states_.PushBack(state_ptr);
    ++size_;
  }

  void Clear() override {
    states_.Clear();
    size_ = 0;
  }

  bool IsEmpty() const override {
    return states_.IsEmpty();
  }

  size_t Size() const override {
    return size_;
  }

 private:
  wheels::IntrusiveList<State> states_;
  size_t size_{0};
};

/////////////////////////////////////////////////////////////////////

}  // namespace fork
