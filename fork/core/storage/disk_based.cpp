#include <fork/core/storage/disk_based.hpp>

#include <wheels/support/assert.hpp>
#include <wheels/support/env.hpp>

#include <algorithm>
#include <string>

#include <fcntl.h>
#include <zconf.h>
#include <sys/mman.h>
#include <sys/stat.h>

namespace storage {

static const size_t kMaxSize = (size_t)1024 * 1024 * 1024 * 20;
static const size_t kPageSize = 4096;
static const size_t kInitSize = kPageSize * kInitPages;

void DiskBasedAllocator::MmappedFile::Init(const std::string& dir) {
  fd_ = open(dir.c_str(), O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
  ftruncate(fd_, kInitSize);

  char* start = static_cast<char*>(mmap(/*addr=*/nullptr,
                                        /*length=*/kMaxSize,
                                        /*prot=*/PROT_READ | PROT_WRITE,
                                        /*flags=*/MAP_SHARED,
                                        /*fd=*/fd_, /*offset=*/0));
  data_ = {start, kInitSize};
}

void DiskBasedAllocator::Init() {
  std::string dir1 = wheels::GetTempPath().value_or("/tmp") + "/fork/";
  struct stat st;
  if (stat(dir1.c_str(), &st) == -1) {
    mkdir(dir1.c_str(), 0700);
  }

  old_.Init(dir1 + "tmp1");
  new_.Init(dir1 + "tmp2");
}

static const size_t kAdd = (size_t)1024 * 1024 * 1024;
char* DiskBasedAllocator::Allocate(size_t size) {
  EnsureSpace(size);
  size_t offset = new_.bytes_allocated_;
  new_.bytes_allocated_ += size;
  return new_.data_.Begin() + offset;
}

void DiskBasedAllocator::ReleaseOld() {
  std::swap(old_, new_);
  new_.bytes_allocated_ = 0;
}

void DiskBasedAllocator::Free(void* /*addr*/) {
  // nothing
}

void DiskBasedAllocator::EnsureSpace(size_t size) {
  size_t cur_size = new_.data_.Size();
  if (new_.bytes_allocated_ + size > cur_size) {
    size_t new_size = cur_size + kAdd;
    WHEELS_VERIFY(new_size <= kMaxSize, "Exceeded disk size");

    new_.data_ = {new_.data_.Begin(), new_size};
    ftruncate(new_.fd_, new_size);
  }
}

DiskBasedAllocator disk;

void DiskInit() {
  disk.Init();
}

char* DiskAllocate(size_t size) {
  return disk.Allocate(size);
}

void DiskReleaseOld() {
  disk.ReleaseOld();
}

void DiskFree(void* addr) {
  disk.Free(addr);
}

}  // namespace storage
