#pragma once

#include <wheels/memory/view.hpp>

#include <string>

namespace storage {

static const size_t kInitPages = 8;

class DiskBasedAllocator {
 public:
  void Init();

  char* Allocate(size_t size);

  void Free(void* addr);

  void ReleaseOld();

 private:
  void EnsureSpace(size_t size);

 private:
  struct MmappedFile {
    void Init(const std::string& dir);

    wheels::MutableMemView data_;
    int fd_{0};
    size_t bytes_allocated_{0};
  };
  MmappedFile old_;
  MmappedFile new_;
};

void DiskInit();
char* DiskAllocate(size_t size);
void DiskFree(void* addr);
void DiskReleaseOld();

}  // namespace storage
