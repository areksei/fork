#pragma once

#include <fork/core/storage/pointer_storage.hpp>

#include <fork/core/state_queue.hpp>

#include <unordered_set>
#include <memory>

namespace storage {

class SnapshotStorage {
  using StateQueuePtr = std::unique_ptr<fork::StateQueue>;

  static const size_t kVisitedSize = 1 << 25;

 public:
  SnapshotStorage() {
    visited_.reserve(kVisitedSize);
  }

  bool HasMore() const {
    return !states_->IsEmpty();
  }

  size_t Size() const {
    return states_->Size();
  }

  bool Contains(hash::THash state_hash) {
    if (visited_.find(state_hash) != visited_.end()) {
      ++duplicate_count_;
      return true;
    }
    visited_.insert(state_hash);
    ++state_count_;
    return false;
  }

  fork::State Pop() {
    return states_->Pop();
  }

  void Push(const fork::State& state) {
    states_->Push(state);
    if (states_->Size() > max_queue_size_) {
      max_queue_size_ = states_->Size();
    }
  }

  PointerStorage& StackStorage() {
    return stack_snapshots_;
  }

  PointerStorage& AllocatorStorage() {
    return allocator_snapshots_;
  }

  void ForgetFront() {
    stack_snapshots_.Clear();
    allocator_snapshots_.Clear();
  }

  void PrintStatistics() const {
    std::cout << "States Found " << state_count_ + duplicate_count_
              << std::endl;
    std::cout << "Distinct States " << state_count_ << std::endl;
    std::cout << "Queue size " << Size() << std::endl;
    std::cout << "Max queue size " << max_queue_size_ << std::endl;
    std::cout << "Duplicates "
              << duplicate_count_ * 1.0 / (state_count_ + duplicate_count_) *
                     100
              << "%" << std::endl;
    std::cout << "Saved stack snapshots " << stack_snapshots_.Saved()
              << std::endl;
    std::cout << "Saved allocator snapshots " << allocator_snapshots_.Saved()
              << std::endl;
  }

 private:
  StateQueuePtr states_{std::make_unique<fork::PtrStateQueue>()};

  std::unordered_set<size_t> visited_;
  PointerStorage stack_snapshots_;
  PointerStorage allocator_snapshots_;

  size_t state_count_{0};
  size_t duplicate_count_{0};
  size_t max_queue_size_{0};
};

}  // namespace storage
