#include <fork/core/trace/backtrace.hpp>

#include <wheels/support/terminal.hpp>

#if defined(FORK_TRACE)
#include <backward.hpp>

void PrettyPrint(std::string& function) {
  static const std::string kAwkward = "fu2::abi_400::detail::function"
      "<fu2::abi_400::detail::config<true, false, fu2::capacity_default>, "
      "fu2::abi_400::detail::property<true, false, void ()> >";
  static const std::string kPretty = "UniqueFunction<...>";
  size_t res = function.find(kAwkward);

  if (res != std::string::npos) {
    function = function.substr(0, res) + kPretty +
               function.substr(res + kAwkward.size());
  }
}

#if APPLE

void LoadBacktrace(std::string& object, std::vector<std::string>& functions,
                   std::vector<std::string>& traces) {
  static const size_t kSkip = 8;
  static const size_t kDepth = kSkip + 8;

  backward::StackTrace st;
  st.load_here(kDepth);
  st.skip_n_firsts(kSkip);

  backward::TraceResolver tr;
  tr.load_stacktrace(st);

  for (size_t i = 0; i < st.size(); ++i) {
    backward::ResolvedTrace trace = tr.resolve(st[i]);
    PrettyPrint(trace.object_function);

    std::ostringstream os;

    os << "#"
        << i
        << " " << trace.object_function << " [" << trace.addr << "]"
        << std::endl;

    object = trace.object_filename;
    functions.push_back(trace.object_function);
    traces.emplace_back(os.str());
  }
}

#elif LINUX

void LoadBacktrace(std::string& object, std::vector<std::string>& functions,
                   std::vector<std::string>& traces) {
  static const size_t kDepth = 32;

  backward::StackTrace st;
  st.load_here(kDepth);

  backward::TraceResolver tr;
  tr.load_stacktrace(st);
  std::vector<std::string> ignored_paths{"include/c++", "fork/core",
                                         "fork/wrappers", "_deps/"};
  backward::SnippetFactory snippets;

  for (size_t i = 0; i < st.size(); ++i) {
    backward::ResolvedTrace trace = tr.resolve(st[i]);
    bool ignore = false;
    for (auto& ignored : ignored_paths) {
      if (trace.source.filename.find(ignored) != std::string::npos) {
        ignore = true;
        break;
      }
    }
    if (ignore) {
      continue;
    }

    auto lines =
        snippets.get_snippet(trace.source.filename, trace.source.line, 3);
    if (lines.empty()) {
      continue;
    }

    PrettyPrint(trace.object_function);

    std::ostringstream os;
    os << "Object \"" << trace.source.filename << "\""
       << ",\nin " << trace.object_function << "\n";

    for (auto& line : lines) {
      std::string prefix = "  ";
      if (line.first == trace.source.line) {
        prefix = "> ";
        os << wheels::terminal::Magenta();
      }
      os << prefix << line.first << ": " << line.second << std::endl;
      os << wheels::terminal::Reset();
    }

    object = trace.object_filename;
    functions.push_back(trace.object_function);
    traces.emplace_back(os.str());
  }
}

#endif

#endif
