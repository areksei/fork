#include <fork/core/trace/description.hpp>

#include <wheels/support/terminal.hpp>

#include <sstream>

namespace trace {

std::string StateDescription::Finalize(const std::string& indent) {
  std::ostringstream os;
  std::string prefix = /*highlighted_ ? "[*] " :*/ "    ";
  os << indent << prefix << name_ << " {" << std::endl;

  for (auto& d : descriptions_) {
    os << d.Finalize(indent + "\t") << std::endl;
  }

  for (auto& line : lines_) {
    std::string line_prefix = "    ";
    if (line.highlighted_) {
      line_prefix = "[*] ";
      os << wheels::terminal::Red();
    }
    os << indent << "\t" << line_prefix << line.text_ << std::endl;
    os << wheels::terminal::Reset();
  }

  os << indent << "    }" << std::endl;
  return os.str();
}

bool StateDescription::Same(const StateDescription& other) {
  return name_ == other.name_ &&
         descriptions_.size() == other.descriptions_.size() &&
         lines_.size() == other.lines_.size();
}

void StateDescription::HighlightDiff(const StateDescription& other) {
  if (!Same(other)) {
    highlighted_ = true;
    // return;
  }

  for (size_t i = 0; i < descriptions_.size(); ++i) {
    if (i == other.descriptions_.size()) {
      highlighted_ = true;
      break;
    }
    descriptions_[i].HighlightDiff(other.descriptions_[i]);
  }

  for (size_t i = 0; i < lines_.size(); ++i) {
    if (i == other.lines_.size()) {
      highlighted_ = true;
      break;
    }
    if (lines_[i].text_ != other.lines_[i].text_) {
      lines_[i].highlighted_ = true;
    }
  }
}

}  // namespace trace
