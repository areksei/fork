#pragma once

#include <fork/test/detail.hpp>

#include <functional>
#include <string>
#include <vector>

namespace fork {
template <typename T>
class Atomic;
}

namespace trace {

class StateDescription {
 public:
  StateDescription(const std::string& name = "") : name_(name) {
  }

  void SetName(const std::string& name) {
    name_ = name;
  }

  template <typename T, typename std::enable_if<
                            !std::is_integral<T>::value>::type* = nullptr>
  void Add(const std::string& key, const T& value) {
    auto sd = value.DescribeState();
    sd.SetName(key);
    descriptions_.push_back(sd);
  }

  template <typename T, typename std::enable_if<
                            std::is_integral<T>::value>::type* = nullptr>
  void Add(const std::string& key, const T& value) {
    Add(key, std::to_string(value));
  }

  template <typename T>
  void Add(const std::string& key, const fork::Atomic<T>& value) {
    Add(key, value.load());
  }

  template <>
  void Add(const std::string& key, const std::string& value) {
    lines_.emplace_back(Line{key + ": " + value});
  }

  std::string Finalize(const std::string& indent = "");

  bool Same(const StateDescription& other);

  void HighlightDiff(const StateDescription& other);

 private:
  struct Line {
    Line(const std::string& text) : text_(text) {
    }

    std::string text_{};
    bool highlighted_{false};
  };

 private:
  std::string name_{};
  bool highlighted_{false};
  std::vector<Line> lines_;
  std::vector<StateDescription> descriptions_;
};

}  // namespace trace
