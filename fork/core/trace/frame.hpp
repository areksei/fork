#pragma once

#include <string>
#include <vector>

namespace trace {

std::vector<std::string> LoadDebuggerInfo(
    const std::vector<std::string>& functions);

}
