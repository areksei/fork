#include <fork/core/trace/trace.hpp>
#include <fork/core/trace/frame.hpp>
#include <fork/core/trace/backtrace.hpp>
#include <fork/core/checker.hpp>

#include <wheels/support/terminal.hpp>

#include <iomanip>

using namespace std::literals::chrono_literals;

namespace trace {

//////////////////////////////////////////////////////////////////////

struct NewLined {
  std::string_view str_;
};

std::ostream& operator<<(std::ostream& out, const NewLined& q) {
  out << std::endl << q.str_ << std::endl;
  return out;
}

NewLined NewLined(const std::string_view str) {
  return {str};
}

#define NEW_LINED(str) std::endl << str << std::endl

//////////////////////////////////////////////////////////////////////

std::string Line(char symbol, size_t width = 100) {
  std::ostringstream os;

  os << std::setfill(symbol) << std::setw(width) << "";

  return os.str();
}

std::string Centered(const std::string& header, size_t width = 100) {
  std::ostringstream os;

  os << std::setfill(' ') << std::setw((width + header.length()) / 2) << header;

  return os.str();
}

std::string Header(char symbol, const std::string& header, size_t width = 100) {
  std::ostringstream os;

  os << NewLined(Line(symbol, width));
  os << Centered(header, width);
  os << NewLined(Line(symbol, width));

  return os.str();
}

//////////////////////////////////////////////////////////////////////

#if defined(FORK_TRACE)

void Tracer::LoadTrace() {
  traces_.clear();
  functions_.clear();

  if (fork::GetCurrentFiber() != nullptr &&
      fork::GetCurrentFiber()->State() == fork::FiberState::Terminated) {
    return;
  }

  fork::AllocationGuard guard;
  LoadBacktrace(object_, functions_, traces_);
}

#else

void Tracer::LoadTrace() {
}

#endif

void Tracer::LoadLocals() {
  if (!object_.empty() && show_locals_) {
    locals_ = LoadDebuggerInfo(functions_);
  }
}

void Tracer::PrintState() {
  if (!describer_) {
    return;
  }

  StateDescription description = describer_();
  description.HighlightDiff(prev_);

  Out() << RED(NewLined("SHARED STATE:"));
  Out() << NewLined(description.Finalize());

  prev_ = description;

  Out() << wheels::terminal::Reset();
  Out() << CYAN(Line('-')) << std::endl;
}

void Tracer::AddPrinter(Describer printer) {
  describer_ = std::move(printer);
}

void Tracer::TraceBefore() {
  fork::AllocationGuard g;

  if (!FirstSubstep()) {
    return;
  }

  PrintSubstep();
  LoadAndPrintStack();  // or ShowState?
}

void Tracer::TraceAfter() {
  bool done = !fork::GetCurrentFiber()->Enabled();
  auto descriptor = fork::GetCurrentFiber()->Descriptor();
  std::string where = fork::GetCurrentFiber()->Where();
  /*bool terminated =
      fork::GetCurrentFiber()->State() == fork::FiberState::Terminated;*/

  /*if (GetCurrentFiber()->State() == FiberState::Terminated) {
    return;
  }*/

  fork::AllocationGuard g;

  PrintNotes();

  PrintSubstep();

  if (done) {
    Out() << std::endl
          << "STATE: "
          << "RUNNING"
          << " -> " << StateString(fork::ToState(descriptor)) << std::endl;
    if (fork::ToState(descriptor) == fork::FiberState::Suspended) {
      Out() << std::endl
            << "Blocked by " << where << " ("
            << fork::WaitQueueString(descriptor) << ")" << std::endl;
    }
    /*if (terminated) {
      return;
    }*/
    Out() << std::endl << CYAN(Line('-')) << std::endl;
  }

  ShowState();
}

void Tracer::PrintStack() {
  Out() << MAGENTA(NewLined("STACK:"));

  Out() << std::endl;
  for (size_t i = 0; i < traces_.size(); ++i) {
    Out() << traces_[i];
    PrintLocals(i);
    if (i != traces_.size() - 1) {
      Out() << CYAN(NewLined("----------------")) << std::endl;
    }
  }

  Out() << wheels::terminal::Reset();
}

void Tracer::PrintLocals(size_t i) {
  if (locals_.size() <= i) {
    return;
  }

  if ("No locals.\n" != locals_[i] &&
      std::count(locals_[i].begin(), locals_[i].end(), '\n') <= 10) {
    Out() << NewLined("Local variables: ");
    Out() << locals_[i];
  }
}

void Tracer::PrintNotes() {
  if (notes_.rdbuf()->in_avail() != 0) {
    Out() << CYAN(Header('-', "RUNNING"));
    Out() << notes_.str();

    notes_.str("");
    notes_.clear();
  }
}

void Tracer::StepBegin() {
  if (!FirstSubstep()) {
    return;
  }

  ++step_;

  Out() << YELLOW(Header('=', "STEP " + std::to_string(step_)));

  Out() << YELLOW(NewLined("THREAD: " + std::to_string(path_[index_])));

  if (fibers_[path_[index_]]->State() == fork::FiberState::Starting) {
    PrintSubstep();
    Out() << NEW_LINED("STATE: " << StateString(CurrentFiber()->State()));
  }

  /*if (fibers_[fiber_id]->GetRandom() != -1) {
    Out() << "random = " << fibers_[fiber_id]->GetRandom() << std::endl;
  }*/
}

void Tracer::PrintSubstep() {
  std::string header;

  if (FirstSubstep()) {
    header = "BEFORE";
    ++sub_step_;
  } else if (LastSubstep()) {
    header = "AFTER";
    sub_step_ = 0;
  } else {
    header = "SNAPSHOT " + std::to_string(sub_step_);
    ++sub_step_;
  }

  Out() << CYAN(Header('-', header));
}

//////////////////////////////////////////////////////////////////////

void Tracer::ShowNote(const std::string& note) {
  fork::AllocationGuard g;
  notes_ << NewLined(note);
}

void Tracer::ShowLocalState() {
  fork::AllocationGuard g;
  LoadAndPrintStack();
}

void Tracer::ShowState() {
  fork::AllocationGuard g;

  PrintState();
  LoadAndPrintStack();
}

void Tracer::ShowLocals(bool flag) {
  show_locals_ = flag;
}

void Tracer::Finish(const std::string& message) {
  Out() << RED(Header('=', "ASSERTION FAILED"));
  Out() << NEW_LINED(message);
  Out() << RED(NewLined(Line('='))) << std::endl;
}

void Tracer::LoadAndPrintStack() {
  LoadTrace();
  LoadLocals();
  PrintStack();
}

void Tracer::StorePath(std::vector<int> path) {
  path_ = path;
  random_.resize(path.size(), -1);

  if (path_.empty()) {
    Out() << "Violation path not found" << std::endl;
    return;
  }

  os_ = &std::cout;

  Out() << YELLOW(Header('=', "THREAD SWITCHING HISTORY"));

  Out() << std::endl;
  for (auto i : path_) {
    Out() << i << " ";
  }
  Out() << std::endl;

  Out() << cached_.str();

  for (size_t i = 0; i < path_.size(); ++i) {
    if (path_[i] / 10 != 0) {
      int random = path_[i] % 10;
      path_[i] = path_[i] / 10;
      random_[i] = random;
    }
  }
}

fork::Fiber* Tracer::Next() {
  ++index_;
  if (path_.empty() || index_ >= path_.size()) {
    return nullptr;
  }

  StepBegin();
  CurrentFiber()->SetRandom(random_[index_]);
  return CurrentFiber();
}

bool Tracer::FirstSubstep() {
  return sub_step_ == 0;
}

bool Tracer::LastSubstep() {
  return index_ == path_.size() - 1 || path_[index_] != path_[index_ + 1];
}

fork::Fiber* Tracer::CurrentFiber() {
  return fibers_[path_[index_]];
}

//////////////////////////////////////////////////////////////////////

}  // namespace trace
