#pragma once

#include <fork/core/trace/description.hpp>

#include <string>
#include <vector>
#include <sstream>
#include <functional>
#include <iostream>

namespace fork {
class Fiber;
}

namespace trace {

class Tracer {
 public:
  Tracer(std::vector<fork::Fiber*>& fibers) : fibers_(fibers) {
  }

  void StorePath(std::vector<int> path);
  fork::Fiber* Next();

  void PrintState();

  void AddPrinter(Describer printer);

  void TraceBefore();
  void TraceAfter();

  void StepBegin();

  void Finish(const std::string& message);

  void ShowNote(const std::string& note);
  void ShowLocalState();
  void ShowState();

  void ShowLocals(bool flag = true);

 private:
  void LoadTrace();
  void LoadLocals();

  void PrintStack();
  void PrintLocals(size_t i);
  void PrintNotes();

  void LoadAndPrintStack();

  void PrintSubstep();

  bool FirstSubstep();
  bool LastSubstep();

  fork::Fiber* CurrentFiber();

  std::ostream& Out() {
    return *os_;
  }

 private:
  std::stringstream cached_;
  std::ostream* os_{&cached_};

  std::vector<fork::Fiber*>& fibers_;

  std::vector<int> path_{0};
  std::vector<int> random_{0};
  size_t index_{static_cast<size_t>(-1)};

  size_t step_{0};
  size_t sub_step_{0};

  Describer describer_;
  StateDescription prev_;
  std::string object_{};

  std::vector<std::string> functions_;
  std::vector<std::string> traces_;
  std::vector<std::string> locals_;

  std::stringstream notes_{};

  bool show_locals_{true};
};

}  // namespace trace