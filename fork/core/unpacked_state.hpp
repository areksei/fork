#pragma once

#include <context/context.hpp>
#include <fork/core/fiber.hpp>

namespace fork {

class UnpackedState {
 public:
 private:
  context::ExecutionContext loop_context_;

  FiberPtrs fibers_;
  size_t existing_fiber_count_{0};
};

}  // namespace fork
