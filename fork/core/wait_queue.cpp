#include <fork/core/wait_queue.hpp>

#include <fork/core/fiber.hpp>
#include <fork/core/checker.hpp>

#include <fork/test/random.hpp>
#include <fork/test/trace.hpp>

namespace fork {

static inline void Suspend(FiberDescriptor ptr) {
  GetCurrentChecker()->Suspend(ptr);
}

static inline void Resume(Fiber* f) {
  GetCurrentChecker()->Resume(f);
}

void WaitQueue::Park() {
  Suspend(this_);
  FORK_INTERNAL_NOTE("Was woken up by thread "
                     << GetCurrentFiber()->Initiator());
}

void WaitQueue::WakeOne() {
  auto fibers = GetSuspendedFibers();
  if (fibers.empty()) {
    return;
  }
  size_t index = Random(fibers.size() - 1);
  fibers[index]->Initiator() = GetFiberId();
  Resume(fibers[index]);

  FORK_INTERNAL_NOTE("Wake thread " << fibers[index]->Id() << " ("
                                    << SHOW_QUEUE(this_) << ")");
}

void WaitQueue::WakeAll() {
  FORK_INTERNAL_NOTE("Wake all threads from " << SHOW_QUEUE(this_));

  auto fibers = GetSuspendedFibers();
  for (auto f : fibers) {
    f->Initiator() = GetFiberId();
    Resume(f);
  }
}

bool WaitQueue::IsEmpty() const {
  return GetSuspendedFibers().empty();
}

FiberPtrs WaitQueue::GetSuspendedFibers() const {
  return GetCurrentChecker()->GetSuspendedFibers(this_);
}

}  // namespace fork
