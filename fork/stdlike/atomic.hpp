#pragma once

#include <fork/wrappers/atomic.hpp>

namespace fork {
namespace stdlike {

template <typename T>
using atomic = fork::Atomic<T>;  // NOLINT

}  // namespace stdlike
}  // namespace fork
