#pragma once

#include <fork/wrappers/condvar.hpp>

namespace fork {
namespace stdlike {

using condition_variable = fork::ConditionVariable;  // NOLINT

}  // namespace stdlike
}  // namespace fork
