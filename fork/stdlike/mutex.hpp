#pragma once

#include <fork/wrappers/mutex.hpp>

namespace fork {
namespace stdlike {

using mutex = fork::Mutex;  // NOLINT

}  // namespace stdlike
}  // namespace fork
