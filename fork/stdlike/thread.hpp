#pragma once

#include <fork/wrappers/thread.hpp>

#include <fork/core/fiber.hpp>

namespace fork {
namespace stdlike {

using thread = fork::Thread;  // NOLINT

namespace this_thread {

inline void yield() {  // NOLINT
  Yield();
}

}  // namespace this_thread

}  // namespace stdlike
}  // namespace fork
