#include <fork/core/checker.hpp>

#include <fork/test/checker.hpp>

namespace fork {

static CheckerView checker;

CheckerView& GetChecker() {
  return checker;
}

CheckerView& CheckerView::AddInvariant(MessagePredicate predicate) {
  GetCurrentChecker()->AddInvariant(std::move(predicate));
  return *this;
}

CheckerView& CheckerView::Prune(Predicate predicate) {
  GetCurrentChecker()->AddPrune(std::move(predicate));
  return *this;
}

CheckerView& CheckerView::ExpectDeadlock() {
  GetCurrentChecker()->ExpectDeadlock();
  return *this;
}

CheckerView& CheckerView::FailsExpected() {
  GetCurrentChecker()->FailsExpected();
  return *this;
}

CheckerView& CheckerView::PrintState(trace::Describer describer) {
  GetCurrentChecker()->PrintState(std::move(describer));
  return *this;
}

CheckerView& CheckerView::ShowLocals(bool flag) {
  GetCurrentChecker()->GetTracer().ShowLocals(flag);
  return *this;
}

CheckerView& CheckerView::SetDepth(size_t depth) {
  GetCurrentChecker()->SetDepth(depth);
  return *this;
}

}