#pragma once

#include <fork/test/detail.hpp>

namespace fork {

struct CheckerView {

  // Options for Explore mode

  CheckerView& AddInvariant(MessagePredicate predicate); //todo: what to return?

  using Predicate = std::function<bool()>;
  CheckerView& Prune(Predicate predicate);

  CheckerView& ExpectDeadlock();
  CheckerView& FailsExpected();

  CheckerView& SetUIntAtomicBound(UInt value);

  CheckerView& SetDepth(size_t depth);

  // Options for Trace mode

  CheckerView& PrintState(trace::Describer describer);
  CheckerView& ShowLocals(bool flag = true);
};

CheckerView& GetChecker();

}