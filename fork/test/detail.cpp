#include <fork/test/detail.hpp>
#include <fork/test/fork.hpp>
#include <fork/test/random.hpp>

#include <fork/core/checker.hpp>

#include <string>

namespace fork {

/////////////////////////////////////////////////////////////////////

#if defined(FORK_TRACE)

void ShowNote(const std::string& note, bool internal) {
  if (!IsRunningInChecker() ||
      (internal && GetCurrentChecker()->ForksDisabled())) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowNote(note);
}

void ShowLocalState() {
  if (!IsRunningInChecker() || GetCurrentChecker()->ForksDisabled()) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowLocalState();
}

void ShowState() {
  if (!IsRunningInChecker() || GetCurrentChecker()->ForksDisabled()) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowState();
}

#else

void ShowNote(const std::string& /*note*/) {
}

void ShowLocalState() {
}

void ShowState() {
}

#endif

/////////////////////////////////////////////////////////////////////

size_t Random(size_t max) {
  return fork::GetCurrentChecker()->Random(max);
}

bool Either() {
  return Random(1) == 1;
}

/////////////////////////////////////////////////////////////////////

void RunChecker(FiberRoutine main) {
  Checker checker;
  checker.Run(&main);
}

void OnFailure(const std::string& s) {
  fork::GetCurrentChecker()->OnFailure(s);
}

}  // namespace fork
