#pragma once

#include <wheels/support/preprocessor.hpp>
#include <wheels/support/env.hpp>

#include <functional>

namespace trace {

class StateDescription;
using Describer = std::function<StateDescription()>;

}  // namespace trace

namespace fork {

using FiberRoutine = std::function<void()>;
using FiberId = size_t;

// test.hpp

void Trace(const std::string& path);

inline std::string GetTracePath(const char* test_name) {
  return std::string(wheels::GetTempPath().value_or("/tmp") + "/fork/tests/") +
         test_name + ".log";
}

using FiberRoutine = std::function<void()>;
void RunChecker(FiberRoutine main);

using MessagePredicate = std::function<std::pair<bool, std::string>()>;
struct Invariant {
  MessagePredicate predicate_;
  bool fail_expected_{false};

  void ExpectFail() {
    fail_expected_ = true;
  }
};

using UInt = size_t;

void OnFailure(const std::string& s);

// trace.hpp

#if defined(FORK_TRACE)
void ShowNote(const std::string& note, bool internal = false);
#endif

// fork.hpp

#define WITH_FORKS(flag)                                 \
  if (fork::ForkGuard UNIQUE_NAME(__fork){flag}; true) { \
    goto CONCAT(__label, __LINE__);                      \
  } else                                                 \
    CONCAT(__label, __LINE__) :

}  // namespace fork
