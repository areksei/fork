#pragma once

#include <fork/test/detail.hpp>

namespace fork {

// Enqueues new state into the checker queue
void Fork();

// RAII object that turns forks off
class ForkGuard {
 public:
  ForkGuard(bool forks_disabled = true);
  ~ForkGuard();

 private:
  bool prev_preempt_;
};

// Wraps object methods making them atomic
// Usage:
// ForkGuarded<SomeObj> obj_;
// obj_->ObjMethod();
template <typename T>
class ForkGuarded {
 private:
  class Guarded : public ForkGuard {
   public:
    explicit Guarded(T& object) : object_(object) {
    }

    T* operator->() {
      return &object_;
    }

   private:
    T& object_;
  };

 public:
  template <typename... Args>
  ForkGuarded(Args&&... args) : object_(std::forward<Args>(args)...) {
  }

  Guarded operator->() {
    Fork();
    return Guarded(object_);
  }

 private:
  T object_;
};

// Usage:
// ATOMICALLY {
//  <code that executes atomically>
// }
#define ATOMICALLY WITH_FORKS(true)

// Undoes effects of any Fork guards
#define FORCE_FORKS WITH_FORKS(false)

}  // namespace fork
