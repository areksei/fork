#pragma once

namespace fork {

// Generates max+1 execution states
// Return value \in [0, max]
size_t Random(size_t max);

// same as Random(1)
// Usage:
// if (Either()) {
// ...
// } else {
// ...
// }
bool Either();

}  // namespace fork
