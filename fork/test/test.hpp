#pragma once

#include <fork/test/detail.hpp>
#include <fork/test/fork.hpp>

#include <sstream>

namespace fork {

////////////////////////////////////////////////////////////////////////////////

#define FORK_TEST(name)                     \
  void TestBody();                          \
  void ForkTest() {                         \
    fork::Trace(fork::GetTracePath(#name)); \
    TestBody();                             \
  }                                         \
  int main() {                              \
    fork::RunChecker(ForkTest);             \
    return 0;                               \
  }                                         \
  void TestBody()

////////////////////////////////////////////////////////////////////////////////

#define FORK_ASSERT(condition, message) \
  do {                                  \
    fork::Fork();                       \
    if (!(condition)) {                 \
      std::stringstream s;              \
      s << message;                     \
      fork::OnFailure(s.str());         \
    }                                   \
  } while (false)

//////////////////////////////////////////////////////////////////////

}  // namespace fork
