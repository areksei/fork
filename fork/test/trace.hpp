#pragma once

#include <fork/core/trace/description.hpp>

#include <wheels/support/map.hpp>

#include <fork/test/detail.hpp>

namespace fork {

////////////////////////////////////////////////////////////////////////////////

#if defined(FORK_TRACE)

#define FORK_NOTE(note)        \
  do {                         \
    std::stringstream __s;     \
    __s << note;               \
    fork::ShowNote(__s.str()); \
  } while (false)

#define FORK_INTERNAL_NOTE(note)     \
  do {                               \
    std::stringstream __s;           \
    __s << note;                     \
    fork::ShowNote(__s.str(), true); \
  } while (false)

#else

#define FORK_NOTE(note)

#define FORK_INTERNAL_NOTE(note)

#endif

void ShowLocalState();

void ShowState();

////////////////////////////////////////////////////////////////////////////////

// Automatic generation of a class description
// Usage: class MyClass {
// ...
// FORK_DESCRIBE(field1, field2, field3)
// ...
// Type1 field1;
// Type2 field2;
// Type3 field3;
// ...
// }
#define FORK_DESCRIBE(...)                        \
  trace::StateDescription DescribeState() const { \
    trace::StateDescription d;                    \
    MAP(FORK_DESCRIBE_ITEM, __VA_ARGS__)          \
    return d;                                     \
  }

#define FORK_DESCRIBE_ITEM(x) d.Add(#x, x);

////////////////////////////////////////////////////////////////////////////////

}  // namespace fork
