#include <fork/wrappers/atomic.hpp>

#include <fork/test/checker.hpp>

namespace fork {

static UInt bound = 0;

CheckerView& CheckerView::SetUIntAtomicBound(UInt value) {
  bound = value;
  return *this;
}

template <>
UInt CutValue(UInt value) {
  if (bound == 0) {
    return value;
  }
  return (value + bound) % bound;
}

}  // namespace fork