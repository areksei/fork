#pragma once

#include <fork/core/checker.hpp>

#include <fork/test/trace.hpp>

namespace fork {

template <typename T>
T CutValue(T value) {
  return value;
}

template <>
UInt CutValue(UInt value);

template <typename T>
class AtomicValue {
 public:
  AtomicValue() = default;
  explicit AtomicValue(T value) : value_(CutValue(value)) {
  }

  void Store(T value) {
    value_ = CutValue(value);
  }

  T Load() const {
    return value_;
  }

  T Exchange(T new_value) {
    return std::exchange(value_, CutValue(new_value));
  }

  bool CompareExchange(T& expected, T desired) {
    desired = CutValue(desired);
    if (value_ == expected) {
      value_ = desired;
      return true;
    }
    expected = desired;
    return false;
  }

  T FetchAdd(T diff) {
    return std::exchange(value_, CutValue(value_ + diff));
  }

  T FetchSub(T diff) {
    return std::exchange(value_, CutValue(value_ - diff));
  }

 private:
  T value_;
};

template <typename T>
class Atomic {
 public:
  Atomic() {
  }

  explicit Atomic(T initial_value) : underlying_(initial_value) {
  }

  // NOLINTNEXTLINE
  T load() const {
    Fork();
    return underlying_.Load();
  }

  operator T() const noexcept {
    return load();
  }

  // NOLINTNEXTLINE
  void store(T value,
             std::memory_order /*unused*/ = std::memory_order_seq_cst) {
    Fork();
    underlying_.Store(value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
  }

  Atomic& operator=(T value) {
    store(value);
    return *this;
  }

  // NOLINTNEXTLINE
  T exchange(T new_value,
             std::memory_order /*order*/ = std::memory_order_seq_cst) {
    Fork();
    const T prev_value = underlying_.Exchange(new_value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("exchange(): prev = " << prev_value);
    return prev_value;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(T& expected, T desired,
                             std::memory_order /*unused*/,
                             std::memory_order /*unused*/) {
    Fork();
    bool succeeded = underlying_.CompareExchange(expected, desired);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("compare_exchange_weak(): succeeded = " << succeeded);
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order order = std::memory_order_seq_cst) {
    return compare_exchange_weak(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(T& expected, T desired,
                               std::memory_order /*unused*/,
                               std::memory_order /*unused*/) {
    Fork();
    bool succeeded = underlying_.CompareExchange(expected, desired);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("compare_exchange_strong(): succeeded = " << succeeded);
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order order = std::memory_order_seq_cst) noexcept {
    return compare_exchange_strong(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  T fetch_add(const T value,
              std::memory_order /*unused*/ = std::memory_order_seq_cst) {
    Fork();
    T prev_value = underlying_.FetchAdd(value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("fetch_add: prev = " << prev_value);
    return prev_value;
  }

  // NOLINTNEXTLINE
  T fetch_sub(const T value,
              std::memory_order /*unused*/ = std::memory_order_seq_cst) {
    Fork();
    T prev_value = underlying_.FetchSub(value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("fetch_sub(): prev = " << prev_value);
    return prev_value;
  }

  T operator++() {
    return fetch_add(1) + 1;
  }

  T operator--() {
    return fetch_sub(1) - 1;
  }

 private:
  AtomicValue<T> underlying_;
};

}  // namespace fork
