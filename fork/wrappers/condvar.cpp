#include <fork/wrappers/condvar.hpp>

#include <fork/core/checker.hpp>
#include <fork/core/wait_queue.hpp>

#include <fork/test/random.hpp>
#include <fork/test/fork.hpp>
#include <fork/test/trace.hpp>

namespace fork {

/////////////////////////////////////////////////////////////////////

class ConditionVariable::Impl {
 public:
  void Wait(Lock& lock) {
    // Spurious wakeup
    if (!GetCurrentChecker()->ForksDisabled() && Either()) {
      FORK_INTERNAL_NOTE("Spurious wakeup in condvar");
      return;
    }

    // ShowNote("Going to park in condvar");

    Fork();
    ATOMICALLY {
      lock.unlock();
    }
    GetCurrentFiber()->Where() = "condvar";
    wait_queue_.Park();

    lock.lock();

    FORK_INTERNAL_NOTE("Exited wait in condvar");
  }

  void NotifyOne() {
    Fork();
    wait_queue_.WakeOne();
  }

  void NotifyAll() {
    Fork();
    wait_queue_.WakeAll();
  }

 private:
  WaitQueue wait_queue_;
};

/////////////////////////////////////////////////////////////////////

ConditionVariable::ConditionVariable()
    : pimpl_(std::make_unique<ConditionVariable::Impl>()) {
}

ConditionVariable::~ConditionVariable() {
}

void ConditionVariable::wait(Lock& lock) {  // NOLINT
  pimpl_->Wait(lock);
}

void ConditionVariable::notify_one() {  // NOLINT
  pimpl_->NotifyOne();
}

void ConditionVariable::notify_all() {  // NOLINT
  pimpl_->NotifyAll();
}

}  // namespace fork
