#pragma once

#include <fork/wrappers/mutex.hpp>

#include <memory>
#include <mutex>

namespace fork {

class ConditionVariable {
 public:
  using Lock = std::unique_lock<Mutex>;

  ConditionVariable();
  ~ConditionVariable();

  void wait(Lock& lock);  // NOLINT

  template <class Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one();  // NOLINT
  void notify_all();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fork
