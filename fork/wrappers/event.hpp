#pragma once

#include <fork/core/wait_queue.hpp>
#include <fork/core/checker.hpp>

#include <fork/test/trace.hpp>

namespace fork {

class Event {
 public:
  void Await() {
    if (!ready_) {
      GetCurrentFiber()->Where() = "event awaiting";
      wait_queue_.Park();
    }
  }

  void Signal() {
    ready_ = true;
    if (!wait_queue_.IsEmpty()) {
      FORK_INTERNAL_NOTE("Event signalled");
      wait_queue_.WakeAll();
    }
  }

 private:
  bool ready_{false};
  WaitQueue wait_queue_;
};

}  // namespace fork
