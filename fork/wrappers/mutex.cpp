#include <fork/wrappers/mutex.hpp>

#include <fork/core/checker.hpp>
#include <fork/core/wait_queue.hpp>

#include <fork/test/trace.hpp>

namespace fork {

/////////////////////////////////////////////////////////////////////

class Mutex::Impl {
 public:
  void Lock() {
    Fork();

    GetCurrentFiber()->Where() = "mutex";

    while (locked_) {
      FORK_INTERNAL_NOTE("Mutex already locked by thread " << owner_id_);
      wait_queue_.Park();
    }
    locked_ = true;

    FORK_INTERNAL_NOTE("Mutex locked");

#if defined(FORK_TRACE)
    owner_id_ = GetFiberId();
#endif
  }

  bool TryLock() {
    Fork();

    if (!locked_) {
      locked_ = true;
#if defined(FORK_TRACE)
      owner_id_ = GetFiberId();
#endif
      FORK_INTERNAL_NOTE("Mutex locked by TryLock");
      return true;
    }

    WHEELS_UNUSED(owner_id_);
    FORK_INTERNAL_NOTE("Mutex TryLock failed, already locked by thread "
                       << owner_id_);
    return false;
  }

  void Unlock() {
    Fork();

    locked_ = false;

#if defined(FORK_TRACE)
    owner_id_ = -1;
#endif

    if (!wait_queue_.IsEmpty()) {
      wait_queue_.WakeOne();
    }

    FORK_INTERNAL_NOTE("Mutex unlocked");
  }

 private:
  bool locked_{false};
  WaitQueue wait_queue_;
  FiberId owner_id_{static_cast<size_t>(-1)};
};

/////////////////////////////////////////////////////////////////////

Mutex::Mutex() : pimpl_(std::make_unique<Mutex::Impl>()) {
}

Mutex::~Mutex() {
}

void Mutex::lock() {  // NOLINT
  pimpl_->Lock();
}

bool Mutex::try_lock() {  // NOLINT
  return pimpl_->TryLock();
}

void Mutex::unlock() {  // NOLINT
  pimpl_->Unlock();
}

}  // namespace fork
