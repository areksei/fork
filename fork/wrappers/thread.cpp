#include <fork/wrappers/event.hpp>
#include <fork/wrappers/thread.hpp>

#include <fork/core/checker.hpp>

#include <fork/test/test.hpp>

namespace fork {

/////////////////////////////////////////////////////////////////////

class Thread::Impl {
 public:
  Impl(FiberRoutine routine) : routine_(std::move(routine)) {
    Spawn([this]() { Run(); });
  }

  ~Impl() {
    FORK_ASSERT(joined_, "Thread was not joined");
  }

  void Join() {
    FORK_INTERNAL_NOTE("Joining thread " << id_);
    if (joined_) {
      FORK_INTERNAL_NOTE("Already joined");
      return;  // already
    }
    done_.Await();
    FORK_INTERNAL_NOTE("Joined");
    joined_ = true;
  }

  bool Joinable() const {
    return true;
  }

 private:
  void Run() {
#if defined(FORK_TRACE)
    id_ = GetFiberId();
#endif

    GetCurrentFiber()->CanonizeValues({(char*)this, sizeof(*this)});
    routine_();
    done_.Signal();
  }

 private:
  FiberRoutine routine_;
  bool joined_{false};
  Event done_;

#if defined(FORK_TRACE)
  FiberId id_;
#endif
};

/////////////////////////////////////////////////////////////////////

Thread::Thread(FiberRoutine routine)
    : pimpl_(std::make_unique<Thread::Impl>(std::move(routine))) {
}

Thread::Thread(Thread&& that) : pimpl_(std::move(that.pimpl_)) {
}

Thread& Thread::operator=(Thread&& that) {
  pimpl_ = std::move(that.pimpl_);
  return *this;
}

Thread::~Thread() {
}

void Thread::join() {  // NOLINT
  pimpl_->Join();
}

bool Thread::joinable() {  // NOLINT
  return pimpl_->Joinable();
}

void Thread::detach() {  // NOLINT
  throw std::runtime_error("detach not implemented");
}

}  // namespace fork
