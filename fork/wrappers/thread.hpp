#pragma once

#include <fork/test/detail.hpp>

#include <memory>
#include <functional>

namespace fork {

class Thread {
 public:
  Thread(FiberRoutine routine);

  Thread(Thread&& other);
  Thread& operator=(Thread&& other);

  ~Thread();

  void join();  // NOLINT

  bool joinable();  // NOLINT

  void detach();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fork
