#include "philosopher.hpp"

#include <fork/test/fork.hpp>

namespace dining {

void Philosopher::EatOneMoreTime() {
  AcquireForks();
  Eat();
  ReleaseForks();
  Think();
}

// Acquires left_fork_ and right_fork_
void Philosopher::AcquireForks() {
  left_fork_.lock();
  right_fork_.lock();
}

void Philosopher::Eat() {
  table_.AccessPlate(seat_);
  table_.AccessPlate(table_.ToRight(seat_));
  ++eat_count_;
}

// Releases left_fork_ and right_fork_
void Philosopher::ReleaseForks() {
  right_fork_.unlock();
  left_fork_.unlock();
}

void Philosopher::Think() {
  fork::Fork();
}

}  // namespace dining
