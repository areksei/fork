#include <fork/stdlike/mutex.hpp>
#include <fork/stdlike/thread.hpp>

#include <fork/test/test.hpp>
#include <fork/test/trace.hpp>
#include <fork/test/checker.hpp>

#include <cmath>
#include <string>

#include "dining.hpp"
#include "philosopher.hpp"

using namespace std::literals::chrono_literals;

using namespace fork::stdlike;

FORK_TEST(Dining) {
  fork::GetChecker()
      .ExpectDeadlock()
      .ShowLocals(false);

  static const size_t kSeats = 4;

  dining::Table table{kSeats};

  std::vector<thread> workers;
  for (size_t i = 0; i < kSeats; ++i) {
    workers.emplace_back([&, i]() {
      dining::Philosopher plato{table, i};
      while (true) {
        plato.EatOneMoreTime();
      }
    });
  }

  for (auto& w : workers) {
    w.join();
  }
}
