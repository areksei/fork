#include <tinyfutures/executors/helpers.hpp>

namespace executors {

void SafelyExecuteHere(Task& task) {
  try {
    task();
  } catch (...) {
    // ¯\_(ツ)_/¯
  }
}

}  // namespace executors
