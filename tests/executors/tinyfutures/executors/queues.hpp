#pragma once

#include <fork/stdlike/mutex.hpp>
#include <fork/stdlike/condition_variable.hpp>
#include <fork/stdlike/atomic.hpp>

#include <deque>
#include <optional>
#include <vector>
#include <algorithm>

using namespace fork::stdlike;

namespace executors {

// Multi-producer/multi-consumer (MPMC) unbounded blocking queue

template <typename T>
class MPMCBlockingQueue {
 public:
  // Returns false iff queue is closed / shutted down
  bool Put(T item) {
    std::lock_guard guard(mutex_);

    if (closed_) {
      return false;
    }

    items_.push_back(std::move(item));
    has_items_.notify_one();
    return true;
  }

  // Await and dequeue next item
  // Returns false iff queue is both 1) drained and 2) closed
  std::optional<T> Take() {
    std::unique_lock lock(mutex_);

    while (items_.empty() && !closed_) {
      has_items_.wait(lock);
    }

    if (!items_.empty()) {
      T item = std::move(items_.front());
      items_.pop_front();

      return std::move(item);
    }

    return std::nullopt;
  }

  // Close queue for producers
  void Close() {
    std::lock_guard guard(mutex_);

    closed_ = true;
    has_items_.notify_all();
  }

  // Close queue for producers and consumers,
  // discard existing items
  void Shutdown() {
    std::lock_guard guard(mutex_);

    closed_ = true;
    items_.clear();
    has_items_.notify_all();
  }

 private:
  std::deque<T> items_;

  condition_variable has_items_;
  mutex mutex_;

  bool closed_{false};
};

//////////////////////////////////////////////////////////////////////

// Multi-producer/single-consumer (MPSC) unbounded lock-free queue

template <typename T>
class MPSCLockFreeQueue {
  struct Node {
    Node(T item, Node* next) : item_(std::move(item)), next_(next) {
    }
    T item_;
    Node* next_;
  };

 public:
  ~MPSCLockFreeQueue() {
    TakeAll();
  }

  void Put(T item) {
    auto head = head_.load();
    auto node = new Node{std::move(item), head};

    while (!head_.compare_exchange_weak(node->next_, node)) {
    }
  }

  std::deque<T> TakeAll() {
    auto head = head_.exchange(nullptr);
    return Unfold(head);
  }

  bool IsEmpty() const {
    return head_.load() == nullptr;
  }

 private:
  static std::deque<T> Unfold(Node* node) {
    std::deque<T> items;
    while (node != nullptr) {
      items.push_front(std::move(node->item_));
      auto to_delete = node;
      node = node->next_;
      delete to_delete;
    }
    return items;
  }

 private:
  atomic<Node*> head_{nullptr};
};

}  // namespace executors
