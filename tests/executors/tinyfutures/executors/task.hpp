#pragma once

#include <tinysupport/function.hpp>

namespace executors {

// Move-only task
// Can wrap move-only lambdas
using Task = tiny::support::UniqueFunction<void()>;

}  // namespace executors
