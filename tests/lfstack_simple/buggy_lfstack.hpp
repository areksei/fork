#pragma once

#include <fork/stdlike/atomic.hpp>

using namespace fork::stdlike;

template <typename T>
class BuggyLockFreeStack {
  struct Node {
    T item_;
    Node* next_{nullptr};

    explicit Node(T item) : item_(std::move(item)) {
    }
  };

 public:
  ~BuggyLockFreeStack() {
    Node* cur = top_.load();

    while (cur != nullptr) {
      Node* tmp = cur->next_;
      delete cur;
      cur = tmp;
    }
  }

  void Push(T item) {
    Node* node = new Node(std::move(item));
    Node* expected_top = top_.load();

    do {
      node->next_ = expected_top;
    } while (!top_.compare_exchange_weak(expected_top, node));
  }

  bool Pop(T& item) {
    Node* top = top_.load();

    do {
      if (top == nullptr) {
        return false;
      }
    } while (!top_.compare_exchange_weak(top, top->next_));

    item = std::move(top->item_);
    delete top;

    return true;
  }

 private:
  atomic<Node*> top_{nullptr};
};

