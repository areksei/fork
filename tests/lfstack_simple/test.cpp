#include <iostream>

#include <fork/test/test.hpp>
#include <fork/test/checker.hpp>

#include <fork/stdlike/thread.hpp>

#include <fork/core/checker.hpp>

#include <cmath>

#include "buggy_lfstack.hpp"

using namespace fork::stdlike;

FORK_TEST(PushPop) {
  static const int kExpected = 5;
  static const size_t kThreads = 3;

  fork::GetChecker()
      .FailsExpected()
      .ShowLocals(false);

  BuggyLockFreeStack<int> stack;

  auto worker = [&]() {
    while (true) {
      stack.Push(kExpected);

      int result;
      bool popped = stack.Pop(result);

      FORK_ASSERT(popped, "empty stack");
      FORK_ASSERT(result == kExpected, "unexpected popped value");
    }
  };

  std::vector<thread> threads;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(worker);
  }

  for (auto& th : threads) {
    th.join();
  }
}
