#include <fork/test/test.hpp>
#include <fork/test/trace.hpp>
#include <fork/test/checker.hpp>

#include <fork/stdlike/thread.hpp>
#include <fork/stdlike/mutex.hpp>

using namespace fork::stdlike;

FORK_TEST(Deadlock) {
  fork::GetChecker()
      .ExpectDeadlock()
      .ShowLocals(false);

  mutex first_mutex;
  mutex second_mutex;

  thread first_thread([&]() {
    first_mutex.lock();
    second_mutex.lock();

    second_mutex.unlock();
    first_mutex.unlock();
  });

  thread second_thread([&]() {
    second_mutex.lock();
    first_mutex.lock();

    first_mutex.unlock();
    second_mutex.unlock();
  });

  first_thread.join();
  second_thread.join();
}
