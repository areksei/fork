#include <iostream>

#include <fork/test/trace.hpp>

#include <fork/stdlike/thread.hpp>

#include <fork/test/test.hpp>
#include <fork/test/checker.hpp>

#include <cmath>
#include <string>

#include "ticket_lock.hpp"

using namespace fork::stdlike;

FORK_TEST(EndlessLoop) {
  const static size_t kThreads = 3;
  TicketLock lock;
  bool in_critical_section = false;

  fork::GetChecker()
      .SetUIntAtomicBound(5)
      .PrintState(
      [&lock, &in_critical_section]() {
        trace::StateDescription d{"LockState"};
        d.Add("TicketLock", lock);
        d.Add("in_critical_section_", in_critical_section);
        return d;
      });

  auto routine = [&]() {
    while (true) {
      lock.Lock();
      FORK_ASSERT(!std::exchange(in_critical_section, true),
                  "Mutual exclusion violated");
      FORK_ASSERT(std::exchange(in_critical_section, false),
                  "Mutual exclusion violated");
      lock.Unlock();
    }
  };

  std::vector<thread> threads;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(routine);
  }

  for (auto& th : threads) {
    th.join();
  }
}
