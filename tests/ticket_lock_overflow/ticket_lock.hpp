#pragma once

#include <fork/stdlike/atomic.hpp>
#include <fork/core/trace/trace.hpp>

using namespace fork::stdlike;

class TicketLock {
 public:
  void Lock() __attribute__((noinline)) {
    const size_t this_thread_ticket = next_free_ticket_.fetch_add(1);

    while (this_thread_ticket != owner_ticket_.load()) {
      // wait
    }
  }

  bool TryLock() {
    size_t expected = owner_ticket_.load();
    return next_free_ticket_.compare_exchange_strong(expected, expected + 1);
  }

  void Unlock() {
    size_t owner = owner_ticket_.load();
    owner_ticket_.store(owner + 1);
  }

  FORK_DESCRIBE(next_free_ticket_, owner_ticket_)

 private:
  atomic<size_t> next_free_ticket_{0};
  atomic<size_t> owner_ticket_{0};
};
